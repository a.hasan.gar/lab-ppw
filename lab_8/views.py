from django.shortcuts import render

AUTHOR = "Ahmad Hasan Siregar"
response = {}

def index(request):
	html = "lab_8/lab_8.html"
	response = {"author" : AUTHOR}
	
	return render(request, html, response)