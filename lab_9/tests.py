import os, environ
from django.test import TestCase
from django.urls import reverse, resolve
from django.http.cookie import SimpleCookie

from .views import index, profile, cookie_login, cookie_profile
import lab_9.api_enterkomputer as enter_komputer

# Create your tests here.
class Lab9UnitTest(TestCase):
	def setUp(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False))
		environ.Env.read_env('.env')
		self.sso_username = env("SSO_USERNAME")
		self.sso_password = env("SSO_PASSWORD")

	# cookie login
	def test_cookie_login(self):
		usr = "Azurey"
		pwd = "pass"
		data = {"username" : usr, "password" : pwd}
		url = "/lab-9/cookie/auth_login/"
		response = self.client.post(url, data)

		cookies = self.client.cookies
		self.assertEqual(cookies["user_login"].value, usr)
		self.assertEqual(cookies["user_password"].value, pwd)
		self.assertRedirects(response, reverse("lab-9:cookie_login"), 302, 302)

	def test_cookie_login_invalid(self):
		# invalid username & password
		usr = "user"
		pwd = "pwd"
		data = {"username" : usr, "password" : pwd}
		url = "/lab-9/cookie/auth_login/"
		response = self.client.post(url, data)

		cookies = self.client.cookies
		self.assertTrue("user_login" not in cookies)
		self.assertTrue("user_password" not in cookies)
		self.assertRedirects(response, reverse("lab-9:cookie_login"), 302, 200)

		# invalid method (expected POST)
		response = self.client.get(url)
		self.assertRedirects(response, reverse("lab-9:cookie_login"), 302, 200)		

	def test_cookie_not_logged_in(self):
		found = resolve("/lab-9/cookie/login/")
		self.assertEqual(found.func, cookie_login)

		response = self.client.get("/lab-9/cookie/login/")
		self.assertEqual(response.status_code, 200)
		html = "lab_9/cookie/login.html"
		self.assertTemplateUsed(response, html)

	# cookie logout
	def test_cookie_logout(self):
		usr = "Azurey"
		pwd = "pass"
		data = {"username" : usr, "password" : pwd}
		login_url = "/lab-9/cookie/auth_login/"
		self.client.post(login_url, data)

		logout_url = "/lab-9/cookie/clear/"
		response = self.client.get(logout_url)

		cookies = self.client.cookies
		self.assertEqual(cookies["user_login"].value, "")
		self.assertEqual(cookies["user_password"].value, "")

	# cookie profile
	def test_cookie_profile(self):
		usr = "Azurey"
		pwd = "pass"
		data = {"username" : usr, "password" : pwd}
		login_url = "/lab-9/cookie/auth_login/"
		self.client.post(login_url, data)

		profile_url = "/lab-9/cookie/profile/"
		found = resolve(profile_url)
		self.assertEqual(found.func, cookie_profile)

		response = self.client.get(profile_url)
		self.assertEqual(response.status_code, 200)
		html = "lab_9/cookie/profile.html"
		self.assertTemplateUsed(response, html)

	def test_cookie_profile_invalid(self):
		# not logged in
		profile_url = "/lab-9/cookie/profile/"
		response = self.client.get(profile_url)
		self.assertRedirects(response, reverse("lab-9:cookie_login"), 302, 200)

		# invalid cookies data
		usr_dummy = "user"
		pwd_dummy = "pwd"
		self.client.cookies = SimpleCookie({'user_login': usr_dummy, 'user_password' : pwd_dummy})
		response = self.client.get(profile_url)
		self.assertEqual(response.status_code, 200)
		html = "lab_9/cookie/login.html"
		self.assertTemplateUsed(response, html)

	# custom_auth
	def test_custom_auth_login(self):
		data = {'username' : self.sso_username, 'password' : self.sso_password}
		response = self.client.post('/lab-9/custom_auth/login/', data)
		self.assertRedirects(response, reverse('lab-9:index'), 302, 302)

		session = self.client.session
		self.assertEqual(session['user_login'], data['username'])
		self.assertTrue('access_token' in session)
		self.assertTrue('kode_identitas' in session)
		self.assertTrue('role' in session)

	def test_custom_auth_login_invalid(self):
		data = {'username' : 'user', 'password' : 'pass'}
		response = self.client.post('/lab-9/custom_auth/login/', data)
		self.assertRedirects(response, reverse('lab-9:index'), 302, 200)
		session = self.client.session
		self.assertTrue('user_login' not in session)
		self.assertTrue('access_token' not in session)
		self.assertTrue('kode_identitas' not in session)
		self.assertTrue('role' not in session)

	def test_custom_auth_logout(self):
		data = {'username' : self.sso_username, 'password' : self.sso_password}
		self.client.post('/lab-9/custom_auth/login/', data)

		response = self.client.get('/lab-9/custom_auth/logout/')
		self.assertRedirects(response, reverse('lab-9:index'), 302)
		session = self.client.session
		self.assertTrue('user_login' not in session)
		self.assertTrue('access_token' not in session)
		self.assertTrue('kode_identitas' not in session)
		self.assertTrue('role' not in session)

	# api_enterkomputer
	def test_api_enterkomputer_available(self):
		drones = enter_komputer.get_drones()
		soundcards = enter_komputer.get_soundcards()
		opticals = enter_komputer.get_opticals()
		self.assertTrue(drones is not None)
		self.assertTrue(soundcards is not None)
		self.assertTrue(opticals is not None)

	# session
	def test_session_not_login(self):
		response = self.client.get('/lab-9/')
		html = 'lab_9/session/login.html'
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, html)

	# session profile
	def test_profile_logged_in(self):
		found = resolve('/lab-9/profile/')
		self.assertEqual(found.func, profile)

		# login
		data = {'username' : self.sso_username, 'password' : self.sso_password}
		self.client.post('/lab-9/custom_auth/login/', data)

		response = self.client.get('/lab-9/profile/')

		self.assertEqual(response.status_code, 200)
		html = 'lab_9/session/profile.html'
		self.assertTemplateUsed(response, html)
		self.assertTrue(response.context['author'], data['username'])

	def test_profile_not_logged_in(self):
		response = self.client.get('/lab-9/profile/')
		self.assertRedirects(response, reverse('lab-9:index'), 302, 200)

	# session items management
	def test_session_add_items(self):
		category = "drones"
		id_1 = "107894"
		id_2 = "107893"
		url = "/lab-9/add_session_item/" + category + "/" + id_1 + "/"
		self.client.get(url)
		url = "/lab-9/add_session_item/" + category + "/" + id_2 + "/"
		self.client.get(url)

		session = self.client.session
		self.assertTrue(id_1 in session[category])
		self.assertTrue(id_2 in session[category])

	def test_session_delete_items(self):
		category = "drones"
		id = "107894"
		add_url = "/lab-9/add_session_item/" + category + "/" + id + "/"
		self.client.get(add_url)
		self.assertTrue(id in self.client.session[category])

		del_url = "/lab-9/del_session_item/" + category + "/" + id + "/"
		self.client.get(del_url)
		self.assertTrue(id not in self.client.session[category])

	def test_session_clear_items(self):
		category_1 = "drones"
		category_2 = "soundcards"
		add_1 = "/lab-9/add_session_item/" + category_1 + "/107894/"
		add_2 = "/lab-9/add_session_item/" + category_2 + "/53496/"
		self.client.get(add_1)
		self.client.get(add_2)

		clear_1 = "/lab-9/clear_session_item/" + category_1 + "/"
		clear_2 = "/lab-9/clear_session_item/" + category_2 + "/"

		self.client.get(clear_1)
		session = self.client.session
		self.assertTrue(category_1 not in session)
		self.assertTrue(category_2 in session)

		self.client.get(clear_2)
		session = self.client.session
		self.assertTrue(category_1 not in session)
		self.assertTrue(category_2 not in session)